#!/usr/bin/env bash
mkdir /tmp/$(date +%Y%m%d_%H%M)_Global_Drap
cd /tmp/$(date +%Y%m%d_%H%M)_Global_Drap
wget https://services.swpc.noaa.gov/images/animations/d-rap/global/d-rap/latest.png
display latest.png &
exit 0
