#!/usr/bin/env bash
mkdir /tmp/$(date +%Y%m%d_%H%M)_Solar-Flares
cd /tmp/$(date +%Y%m%d_%H%M)_Solar-Flares
wget https://sdo.gsfc.nasa.gov/assets/img/latest/latest_1024_0131.jpg
display latest_1024_0131.jpg &
exit 0