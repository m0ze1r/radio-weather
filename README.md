# Radio Weather



## Getting started

Hi! Welcome to "Radio Weather" !

These scripts pull the latest radio weather images from the web and places them neatly into your tmp directory

This utilizes https://prop.kc2g.com/ , https://spaceweatherlive.com , https://sdo.gsfc.nasa.gov and https://www.swpc.noaa.gov/ to display the latest conditions using ImageMagick 

## Dependencies

Please ensure that you have installed on your system
- wget
- ImageMagick
- InkScape

## How do I run the Script?

Make sure that you give execute permissions "chmod -R 755 radio-weather/" before you enter the directory  

- bash conditions.sh -h {to display the help menu}

There are a few flags in this script make sure you read the help menu to find out what you want to view

## What can this script show me?

- "-d" for the current Global D Region Absorption Predictions
- "-np" is for the current North Pole D Region Absorption Predictions
- "-sp" is for the current South Pole D Region Absorption Predictions
- "-m" is for the Maximum Usable Frequency Map
- "-f2" is for the Critical Frequency (foF2) Map.
- "-sr" is for the latest Sunspot Regions Image
- "-ch" is for the latest Coronal Holes Image
- "-sf" is for the latest Solar Flares Image

## Will I add other things in the future to this project

More than likely but this is just a side project at the moment

If you have any other ideas, Please reach me at VK1TTY [AT] whisper [DOT] cat

- Meow