#!/usr/bin/env bash
###
# Variables
###
###
#TimeOfDay=$(date +%Y-%m-%d-%T)
#

while [ ! $# -eq 0 ]
do
    case "$1" in
        --help | -h)
            echo '------------------------------------------------------------------------------'
            echo 'Welcome to the HF Weather Conditions script made by VK1TTY'
            echo '------------------------------------------------------------------------------'
            echo 'This script requires `ImageMagik` to display the images'
            echo 'Please ensure that it is downloaded on your system'
            echo '------------------------------------------------------------------------------'
            echo 'This script downloads images to your /tmp/ directory'
            echo 'and gives them their own directory based on your System Time and Date'
            echo 'Example /tmp/Year Month Day Hour Minute_muf'
            echo '------------------------------------------------------------------------------'
            echo 'This script has a couple of flags shown below.'
            echo '------------------------------------------------------------------------------'
            echo '-h, --help is the help menu'
            echo '------------------------------------------------------------------------------'
            echo '-d is for current Global D Region Absorption Predictions'   
            echo '------------------------------------------------------------------------------'
            echo '-np is for the current North Pole D Region Absorption Predictions'
            echo '------------------------------------------------------------------------------'
            echo '-sp is for the current South Pole D Region Absorption Predictions'
            echo '------------------------------------------------------------------------------'
            echo '-m is for the Maximum Usable Frequency Map'
            echo '------------------------------------------------------------------------------'
            echo '-f2 is for the Critical Frequency (foF2) Map. '
            echo 'This one is simpler than MUF'
            echo 'it`s the highest frequency that you can use for NVIS '
            echo '(skywave communication "in your own backyard")'
            echo '------------------------------------------------------------------------------'
            echo '-sr is for the latest Sunspot Regions Image'
            echo '------------------------------------------------------------------------------'
            echo '-ch is for the latest Coronal Holes Image'
            echo '------------------------------------------------------------------------------'
            echo '-sf is for the latest Solar Flares Image'
            echo '------------------------------------------------------------------------------'
            exit 0
############END OF HELP
            ;;

        --drap | -d )
            echo "Getting the latest Global D Region Absorption Prediction Image"
            if bash scripts/global-drap.sh; then
              echo 'Global DRAP Downloaded'
            else
              exit 1
            fi

            ;;

        --north | -np )
            echo "Getting the latest North Pole Global D Region Absorption Prediction Image"
            if bash scripts/north-pole-drap.sh; then
              echo 'Global DRAP map downloaded'
            else
              exit 1
            fi

            ;;

        --south | -sp )
            echo "Getting the latest South Pole Global D Region Absorption Prediction Image"
            if bash scripts/south-pole-drap.sh; then
              echo 'South Polar DRAP Image downloaded'
            else
              exit 1
            fi

            ;;

        --muf | -m )
            echo "Getting the latest Maximum Usable Frequency Image"
            if bash scripts/muf.sh; then
              echo 'Maximum Usable Frequency Image Downloaded'
            else
              exit 1
            fi

            ;;

        --fof2 | -f2 )
            echo "Getting the latest Critical Frequency (foF2) Image."
            if bash scripts/foF2.sh; then
              echo 'foF2 Downloaded'
            else
              exit 1
            fi

            ;;
        
        --sunspots | -sr)
            echo "Getting the latest Sunspot Regions Image."
            if bash scripts/sunspot-regions.sh; then
              echo 'Sunspot Region Image Downloaded'
            else
              exit 1
            fi

            ;;
        --coronal | -ch)
            echo "Getting the latest Coronal Holes Image."
            if bash scripts/coronal-holes.sh; then
              echo 'Coronal Holes Image Downloaded'
            else
              exit 1
            fi

            ;;

        --flares | -sf)
            echo "Getting the latest Solar Flares Image."
            if bash scripts/solar-flares.sh; then
              echo 'Solar Flares Image Downloaded'
            else
              exit 1
            fi

            ;;


        esac
	shift
done
echo "Script completed"

