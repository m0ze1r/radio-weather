#!/usr/bin/env bash
mkdir /tmp/$(date +%Y%m%d_%H%M)_North_Pole
cd /tmp/$(date +%Y%m%d_%H%M)_North_Pole
wget https://services.swpc.noaa.gov/images/animations/d-rap/north-pole/d-rap/latest.png
display latest.png &
exit 0
