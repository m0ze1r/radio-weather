#!/usr/bin/env bash
mkdir /tmp/$(date +%Y%m%d_%H%M)_Coronal-Holes
cd /tmp/$(date +%Y%m%d_%H%M)_Coronal-Holes
wget https://sdo.gsfc.nasa.gov/assets/img/latest/latest_1024_0193.jpg
display latest_1024_0193.jpg &
exit 0