#!/usr/bin/env bash
mkdir /tmp/$(date +%Y%m%d_%H%M)_foF2
cd /tmp/$(date +%Y%m%d_%H%M)_foF2
wget https://prop.kc2g.com/renders/current/fof2-normal-now.svg
display fof2-normal-now.svg &
exit 0
