#!/usr/bin/env bash
mkdir /tmp/$(date +%Y%m%d_%H%M)_muf
cd /tmp/$(date +%Y%m%d_%H%M)_muf
wget https://prop.kc2g.com/renders/current/mufd-normal-now.svg
display mufd-normal-now.svg &
exit 0