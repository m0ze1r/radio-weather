#!/usr/bin/env bash
mkdir /tmp/$(date +%Y%m%d_%H%M)_Sunspot-Regions
cd /tmp/$(date +%Y%m%d_%H%M)_Sunspot-Regions
wget -U "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)" "https://www.spaceweatherlive.com/images/SDO/SDO_HMIIF_1024.jpg"
display SDO_HMIIF_1024.jpg &
exit 0